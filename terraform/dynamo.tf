resource "aws_dynamodb_table" "dynamodb-table" {
  name = "${var.RESOURCE_PREFIX}skillplay"
  read_capacity = 5
  write_capacity = 5
  hash_key = "Author"
  range_key = "Date"

  attribute {
      name = "Author"
      type = "S"
    }
    attribute {
      name = "Date"
      type = "N"
    }
}