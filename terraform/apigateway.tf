resource "aws_api_gateway_rest_api" "testskillplay-api-gateway" {
  name        = "TestTFAPI"
  description = "API to test tf"
  body        = data.template_file.testskillplay_api_swagger.rendered
}

data "template_file" "testskillplay_api_swagger" {
  template = "${file("api-swagger.yaml")}"

  vars = {
    get_lambda_arn = aws_lambda_function.get-name-lambda.invoke_arn
    post_lambda_arn = aws_lambda_function.post-name-lambda.invoke_arn
  }
}

resource "aws_api_gateway_deployment" "testskillplay-api-gateway-deployment" {
  rest_api_id = aws_api_gateway_rest_api.testskillplay-api-gateway.id
  stage_name  = "test"
}

output "url" {
  value = "${aws_api_gateway_deployment.testskillplay-api-gateway-deployment.invoke_url}/api"
}
