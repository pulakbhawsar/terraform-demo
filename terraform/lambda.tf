
# get
resource "aws_lambda_function" "get-name-lambda" {
  function_name = "${var.RESOURCE_PREFIX}get_name_lambda"
  s3_bucket = var.AWS_S3_BUCKET
  s3_key = "getLambda.zip"

  handler = "index.handler"
  memory_size = 128
  runtime = "nodejs12.x"

  role = aws_iam_role.lambda-iam-role.arn
}

resource "aws_lambda_permission" "api-gateway-invoke-get-lambda" {
  statement_id = "AllowExecutionFromApiGateway"
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.get-name-lambda.arn
  principal = "apigateway.amazonaws.com"

  #source_arn = "${aws_api_gateway_deployment.testskillplay-api-gateway-deployment.execution_arn}/*/*"
  source_arn = "arn:aws:execute-api:${var.AWS_REGION}:${var.AWS_ACCOUNT_ID}:${aws_api_gateway_rest_api.testskillplay-api-gateway.id}/*/*"
}


# post
resource "aws_lambda_function" "post-name-lambda" {
    function_name = "${var.RESOURCE_PREFIX}post_name_lambda"
    s3_bucket = var.AWS_S3_BUCKET
    s3_key = "postLambda.zip"

    handler = "index.handler"
    memory_size = 128
    runtime = "nodejs12.x"

    role = aws_iam_role.lambda-iam-role.arn
  
}

resource "aws_lambda_permission" "api-gateway-invoke-post-lambda" {
  statement_id = "AllowExecutionFromApiGateway"
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.post-name-lambda.arn
  principal = "apigateway.amazonaws.com"

  #source_arn = "${aws_api_gateway_deployment.testskillplay-api-gateway-deployment.execution_arn}/*/*"
  #source_arn = "arn:aws:execute-api:${var.region}:${var.account_id}:${aws_api_gateway_rest_api.test_api.id}/*/${aws_api_gateway_integration.test_test-get-integration.integration_http_method}${aws_api_gateway_resource.test_test.path}"
  source_arn = "arn:aws:execute-api:${var.AWS_REGION}:${var.AWS_ACCOUNT_ID}:${aws_api_gateway_rest_api.testskillplay-api-gateway.id}/*/*"
}