variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}
variable "AWS_REGION" {
  default = "us-east-1"
}
variable "AWS_ACCOUNT_ID" {}

variable "AWS_S3_BUCKET" {}

variable "RESOURCE_PREFIX" {}
