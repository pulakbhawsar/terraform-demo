
# IAM Role
resource "aws_iam_role" "lambda-iam-role" {
  name = "${var.RESOURCE_PREFIX}skillplay_lambda_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
  EOF
}

# Policies

# dynamodb
resource "aws_iam_role_policy" "dynamodb-lambda-policy" {
  name = "${var.RESOURCE_PREFIX}dynamodb_lambda_policy"
  role = aws_iam_role.lambda-iam-role.id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "dynamodb:*"
      ],
      "Resource": "${aws_dynamodb_table.dynamodb-table.arn}"
    }
  ]
}
EOF
}

# cloudwatch policy
resource "aws_iam_role_policy" "cloudwatch-lambda-policy" {
    name = "${var.RESOURCE_PREFIX}cloudwatch_lambda_policy"
    role = aws_iam_role.lambda-iam-role.id
    policy = data.aws_iam_policy_document.api-gateway-logs-policy-document.json
}

# cloudwatch policy document
data "aws_iam_policy_document" "api-gateway-logs-policy-document" {
  statement {
    actions = [ "logs:CreateLogStream",
      "logs:CreateLogGroup",
      "logs:PutLogEvents"
    ] 
    resources = [ "arn:aws:logs:*:*:*" ]
  }
}